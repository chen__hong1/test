# 测试

#### 介绍
这是用于测试的仓库
测试目的：了解用IDEA和Gitee提交实训作业的流程


#### 使用说明

1.  fork本仓库成为自己的仓库，称为仓库A
2.  通过pull request提交作业

#### 使用步骤
1.  本地安装Git
2.  配置IDEA
3.  用IDEA将仓库A导入（clone）到本地成为IDEA的项目
4.  在项目中添加模块，模块名称为学号
5.  通过IDEA将代码commit到本地仓库
6.  通过IDEA将本地仓库push到仓库A
7.  在仓库A通过pull request提交作业


